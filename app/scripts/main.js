$(document).ready(function () {

    let machines = JSON.parse(localStorage.getItem('machines')) || [];
    let UnitsProduced = JSON.parse(localStorage.getItem('UnitsProduced')) || [];
    let machineTemperature = JSON.parse(localStorage.getItem('machineTemperature')) || [];
    let selectedMachine = null;


    // Generate Table of saved machines
    //----------------------------------------------
    function generateDashboard(){
        let table = '<table class="table table-dark"><tr><th>Machines</th><th>Action</th></tr>';
        machines = JSON.parse(localStorage.getItem('machines')) || [];
        for (let i = 0; i < machines.length; i++) {
            table += `<tr>
            <td width="40%">${machines[i].machineName} - <small class="label"><b>${machines[i].machineID}</b></small></td>
            <td>
                <a href="#" data-request="addUnits" data-machine="${machines[i].machineID}" class="btn btn-sm btn-success stats">Add Units</a>
                <a href="#" data-request="setTemp" data-machine="${machines[i].machineID}" class="btn btn-sm btn-success stats">Set Temp.</a>
                <a href="#" data-request="avgU" data-machine="${machines[i].machineID}" class="btn btn-sm btn-secondary stats">Average prod</a>
                <a href="#" data-request="totalU" data-machine="${machines[i].machineID}" class="btn btn-sm btn-secondary stats">Total prod</a>
                <a href="#" data-request="getTemp" data-machine="${machines[i].machineID}" class="btn btn-sm btn-secondary stats">Get Temp.</a>
            </td>
        `
        }
        table +='</table>';
        $('#machineList').html(table);
    }
    generateDashboard();


    // Adding machine handler
    //----------------------------------------------
    $('#addMachineForm').submit(function () {
        let machineName = $('#machineName').val(),
            machineID = $('#machineID').val();

        addMachine(machineID, machineName);
        $('#addMachineModal').modal('hide');
        return false;
    });
    function addMachine(id, name) {
        machines.push({
            machineID: id,
            machineName: name
        });
        localStorage.setItem('machines', JSON.stringify(machines));
        generateDashboard();
    }


    // Adding produced Unit handler
    //----------------------------------------------
    $('#addProducedUnitForm').submit(function () {
        let unitsProduced = $('#unitsProduced').val();

        addProducedUnits(selectedMachine, unitsProduced);
        $('#addProducedUnitModal').modal('hide');
        return false;
    });
    function addProducedUnits(id, number) {
        UnitsProduced.push({
            machineID: id,
            units: number
        });
        localStorage.setItem('UnitsProduced', JSON.stringify(UnitsProduced));
    }
    function getTotalUnitsProduced(id) {
        let total = 0,
            y = 0;

        for (let i = 0; i < UnitsProduced.length; i++) {
            if (id === UnitsProduced[i].machineID) {
                y++; //get the number of entry
                total += Number(UnitsProduced[i].units); // addition units per machine;
            }
        }
        $('#infoModal').find('p.lead').html('The machine <b><small>'+id+'</small></b> total production : <b>'+ total +'</b>');
        $('#infoModal').modal('show');
        return {
            totalUnits: total,
            entry: y
        };
    }
    function getAvrageUnitsProduced(id) {
        let TotalUnitsProduced = getTotalUnitsProduced(id);
        $('#infoModal').find('p.lead').html('The machine <small>'+id+'</small> average production : <b>'+ TotalUnitsProduced.totalUnits / TotalUnitsProduced.entry +'</b>');
        $('#infoModal').modal('show');
    }


    // Machine temp handler
    //----------------------------------------------
    $('#setTempForm').submit(function () {
        let temp = $('#temp').val();

        setTemperature(selectedMachine, temp);
        $('#setTempModal').modal('hide');
        return false;
    });
    function setTemperature(id,temp) {
        machineTemperature.push({
            machineID: id,
            temperature: temp
        });
        localStorage.setItem('machineTemperature', JSON.stringify(machineTemperature));
    }
    function getTemperature(id) {
        let temp = 0;
        for (let i = 0; i < machineTemperature.length; i++) {
            if (id === machineTemperature[i].machineID) {
               temp =  machineTemperature[i].temperature // return machine temperature
            }
        }

        $('#infoModal').find('p.lead').html('The machine <b><small>'+id+'</small></b> temperature : <b>'+ temp +'</b>° deg');
        $('#infoModal').modal('show');
    }


    // Handel action buttons
    //----------------------------------------------
    $('#machineList').on('click','a.stats',function () {
        let request = $(this).data('request');
        selectedMachine =  $(this).data('machine');

        switch (request) {
            case 'addUnits':
                $('#addProducedUnitModal').modal('show');
                break;
            case 'avgU':
                getAvrageUnitsProduced(selectedMachine);
                break;
            case 'totalU':
                getTotalUnitsProduced(selectedMachine);
                break;
            case 'setTemp':
                $('#setTempModal').modal('show');
                break;
            case 'getTemp':
                getTemperature(selectedMachine);
                break;
        }
        return false;
    })


});